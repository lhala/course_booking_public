
let registerForm = document.querySelector("#registerUser");
 registerForm.addEventListener("submit",(e) => {
	event.preventDefault()

let firstName = document.querySelector("#firstName").value
let lastName = document.querySelector("#lastName").value
let email = document.querySelector("#userEmail").value
let mobileNo = document.querySelector("#mobileNumber").value
let password =document.querySelector("#password1").value
let verifyPassword = document.querySelector("#password2").value
	


if((firstName !== "" && lastName !== "" && email !=="" && password !== "")&&(password === verifyPassword) && (mobileNo.length === 11 )){
	//check email-exist
	fetch('https://dry-plains-71651.herokuapp.com/api/users/email-exists',{
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			email: email
		})	
	}).then( res =>{
		return res.json() //read when the response once the responce return to the cient side
	}).then(convertedData =>{
		//control structure to determine the proper procedure depending on the response.
		if (convertedData === false) {
			// allow user to register an accunt
			//crete new account URL for 1st param describe destinaton
		fetch('https://dry-plains-71651.herokuapp.com/api/users/register',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			//API only accepts request in a string format.
			body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					mobileNo: mobileNo,
					password: password

				})
			}).then(res =>{
				console.log(res)
				return res.json()

			}).then(data =>{
				console.log(data)
				//control structure to give out a proper response depending on the return from the backend
				if(data === true){
					Swal.fire({
						icon: "success",
						text: "Registered Successfully"
						
   					}) 
   					setTimeout(function() {
      				window.location.replace('./login.html')
					},2000);
   					
				}else{
					Swal.fire({
						icon: "error",
						 text: "Something Went Wrong during the registration!"

					})
				}
			})	
		}else{
			Swal.fire({
				icon: "warning",
				text:"Email Already Exists!"
			})
		}
	})
}
})
	



