let adminControls = document.querySelector('#adminButton')
let container = document.querySelector('#coursesContainer')

const isAdmin = localStorage.getItem("isAdmin")

if (isAdmin == "false" || !isAdmin) 
{
   adminControls.innerHTML = null;  
} else {
   adminControls.innerHTML = `
     <div class="col-md-2 offset-md-10">
      <a href="./addCourse.html" class="btn btn-block btn-success">Add Course</a>
     </div>
   `
}

fetch('https://dry-plains-71651.herokuapp.com/api/courses').then(res => res.json()).then(jsonData => {
  console.log(jsonData) 
  
    let courseData; 

    if (jsonData.length < 1) {
      console.log("No Courses Available")
        courseData = "No Courses Available"
        container.innerHTML = courseData
    } else {

          courseData = jsonData.map(course => {
          console.log(course._id)
          console.log(course.name)

         let cardFooter; 
        
         if (isAdmin == "false" || !isAdmin) {
            cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Course Details</a>
             `
         }
         else {
            cardFooter =
           `
             <a href="./editCourses.html" class="btn btn-success text-white">Edit Course</a>
             <a href="#" class="btn btn-danger text-white">Delete Course</a>
           `
         }

         //lets fix the current behavior of our courses page that it can only display the last object inserted inside the array. 
         //so far we did not indicate what will be the return of our map()
         return(
              `
             <div class="col-md-6 my-3">
                <div class="card">
                  <div class="card-body">
                    <h3 class="card-title">Course Name: ${course.name}</h3>
                    <p class="card-text text-left">Price: ${course.price}</p>
                    <p class="card-text text-left">Desc: ${course.description}</p>
                    <p class="card-text text-left">Created On: ${course.createdOn}</p>
                  </div>
                  <div class="card-footer">
                    ${cardFooter}
                  </div>
                </div>
             </div>
         ` //you can think of a better message / for dermonstration purposes
          )
      }).join("") //we will use this join() to create a return of a new string. 
         //it "concatenated"/ "combined" all the objects inside the array and converted each to a string data type. 
         container.innerHTML = courseData; 
    }
})
