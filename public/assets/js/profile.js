//get the value of the access token inside the local storage and place it inside a new variable. 
let token = localStorage.getItem("token"); 
// console.log(token)

let lalagyan = document.querySelector("#profileContainer")
//our goal here is to display the information about the user details 
//send a request to the backend project
fetch('https://dry-plains-71651.herokuapp.com/api/users/details', {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
}).then(res => res.json())
.then(jsonData => {
  console.log(jsonData.enrollments)
           jsonData.enrollments.forEach(subject => {
            fetch(`https://dry-plains-71651.herokuapp.com/api/courses/${subject.courseId}`)
            .then(res => res.json())
            .then(data => {
                document.querySelector('.tbody_').insertAdjacentHTML('afterbegin',
                `
                 <tr>
                <td>${data.name}</td>
                <td>${data.description}</td>
                <td>${subject.enrolledOn}</td>
                <td>${subject.status}</td>
                 </tr>
                
                `
            )


        })
                

    })
           
    lalagyan.innerHTML = `
       <div class="col-md-13">
            <section class="jumbotron my-5">
               <h3 class="text-center">First Name: ${jsonData.firstName}</h3>
               <h3 class="text-center">Last Name: ${jsonData.lastName}</h3>
               <h3 class="text-center">Email:     ${jsonData.email}</h3>
               <h3 class="text-center">Mobile Number: ${jsonData.mobileNo}</h3>
               <table class="table">
                <tr>
                    <th>Course Name:</th>
                    <th> Course Descripton</th>
                    <th>Enrolled On:</th>
                    <th>Status: </th>
                    <tbody class="tbody_"></tbody>
                </tr>
               </table>

            </section>
       </div>

    `
})
