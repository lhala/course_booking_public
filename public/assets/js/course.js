
let urlValues = new URLSearchParams(window.location.search)
/*console.log(urlValues)*/
let id = urlValues.get('courseId')
let userId = localStorage.getItem('id')
console.log(userId)
console.log(id)
let token = localStorage.getItem('token')
/*console.log(token)*/

let name   = document.querySelector("#courseName")
let price  = document.querySelector("#coursePrice")
let desc   = document.querySelector("#courseDesc")
let enroll = document.querySelector("#enrollContainer")
      
 	fetch(`https://dry-plains-71651.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData =>{
	console.log(convertedData)

		name.innerHTML = convertedData.name
		price.innerHTML = convertedData.price
		desc.innerHTML = convertedData.description
		enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`

			document.querySelector("#enrollButton").addEventListener( "click",() => {

			//insert course inside enrollements array of the user
			console.log('id:'+ id)
			console.log('userId:' + userId)
			fetch('https://dry-plains-71651.herokuapp.com/api/users/enroll', {
				method:'POST',
				headers:{
					'Content-Type':'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: id,
					userId: userId

				})
			})
			.then(res =>{
				return res.json()
			}).then(convertedResponse =>{
				Swal.fire({
				text: `${convertedResponse.message}`
				
			 })
			setTimeout(function() {
      		window.location.replace('./courses.html')
			},2000);
		})

	})
})
if (token=== null){
	window.location.replace('./login.html')
}