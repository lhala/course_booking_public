let formSubmit = document.querySelector("#createCourse")
formSubmit.addEventListener("submit", (e) => {
	event.preventDefault()
	let name = document.querySelector("#courseName").value
	let cost = document.querySelector("#coursePrice").value
	let desc = document.querySelector("#courseDesc").value
	/*console.log(name);
	console.log(cost);
	console.log(desc);*/

	let course = {}
	if(course === course){
		fetch('https://dry-plains-71651.herokuapp.com/api/courses/course-exists',
		{
			method :'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
			name : name
			})
		}).then (res=>{
			return res.json()
		}).then(info =>{
			if(info === false){
				fetch('https://dry-plains-71651.herokuapp.com/api/courses/create',{

					method: 'POST',
					headers:{
						'Content-Type': 'application/json'
					},
						body: JSON.stringify({
							name: name,
							price: cost,
							description: desc
						})
				
				}).then(res => {
					console.log(res)
					return res.json()
				}).then (info => {
					console.log(info)
					if (info === true) {
						Swal.fire({
						icon: "success",
						text: "New Course Created"
   					})
					setTimeout(function() {
      				window.location.replace('./courses.html')
					},2000);
					} else{
						Swal.fire({
						icon: "error",
						 text: "Something Went Wrong, Check your Credentials!"
						})
					}
				})
			}else{
				Swal.fire({
				icon: "warning",
				text:"Course Already Exists!"
			})
			}
		})
	}
})
