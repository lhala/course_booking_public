let loginForm = document.querySelector('#loginUser')
loginForm.addEventListener("submit", (e) =>{
	event.preventDefault()
let email = document.querySelector("#userEmail").value	
let pass = document.querySelector("#password").value
//validating data for login
if(email =="" || pass ==""){
	alert("Please Enter Email and  Password!")
}else{
	fetch("https://dry-plains-71651.herokuapp.com/api/users/login",{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body:JSON.stringify({
				email: email,
				password: pass
			})
		}).then(res => {
			/*console.log(res)*/
			return res.json()
		}).then(dataConverted =>{
			console.log(dataConverted.accessToken)
			if (dataConverted.accessToken) {
				localStorage.setItem( 'token', dataConverted.accessToken )
				Swal.fire({
				icon:"success",
				text:"You are now Log in"
				})	
				//redirect to another location
				fetch('https://dry-plains-71651.herokuapp.com/api/users/details',{
					headers: {
						//pass on the value of access token
						'Authorization':`Bearer ${dataConverted.accessToken}`
					}
				}).then(res=> {
					return res.json()
				}).then(data => {
					console.log(data)
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					if (data.isAdminn === false) {
						window.location.replace('./profile.html')
					} else {
						window.location.replace('./courses.html')

					}
				
				})
			} else {
				Swal.fire({
					icon:"error",
					text:"Please Register First!"
				})
				setTimeout(() => {
					window.location.replace('./register.html');
				 }, 2000)
			}
			
		})

	}

})

